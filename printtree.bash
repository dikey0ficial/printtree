#!/bin/env bash
set -Eeo pipefail
x=13


printf "%*s" `expr $x - 1` ""
printf "\e[4;33m{}\e[m"
echo
for i in $(seq $x)
do
    printf "\e[1;32m%*s\e[m" $(expr $x - $i + 1) /
    for n in `seq $(expr $(expr $i - 1) \* 2)`
	do
        if [[ $(expr $(expr $n + 1) % 3) = 0 ]]; then
            printf "\e[1;%dm%s\e[m" $(expr 30 + $(expr $RANDOM % 5)) "*"
        else
            printf "\e[1;32m#\e[m"
        fi
    done
    printf "\e[1;32m%s\e[m\n" "\\"
done

for _ in $(seq `expr $x \* 2`)
do
	echo -n "="
done

echo
echo
echo """
 _   _    _    ____  ______   __
| | | |  / \  |  _ \|  _ \ \ / /
| |_| | / _ \ | |_) | |_) \ V / 
|  _  |/ ___ \|  __/|  __/ | |  
|_| |_/_/   \_\_|   |_|    |_|  
                                
  _   _ _______        __ 
 | \ | | ____\ \      / / 
 |  \| |  _|  \ \ /\ / /  
 | |\  | |___  \ V  V /   
 |_| \_|_____|  \_/\_/    
                          
__   _______    _    ____  _ 
\ \ / / ____|  / \  |  _ \| |
 \ V /|  _|   / _ \ | |_) | |
  | | | |___ / ___ \|  _ <|_|
  |_| |_____/_/   \_\_| \_(_)
                             
"""
