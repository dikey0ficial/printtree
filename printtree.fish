#!/bin/env fish

set -f x 13

echo (string pad -w (math $x + 1) (printf "\e[4;33m%s\e[m" {}))
for i in (seq $x)
    echo -n (string pad -w (math $x - $i + 1) (printf "\e[1;32m/\e[m"))
    for n in (seq (math "($i - 1) * 2"))
        if test (math \($n + 1\) % 3) = 0
            printf "\e[1;%dm%s\e[m" (math 30 + \((random) % 5\)) "*"
        else
            printf "\e[1;32m#\e[m"
        end
    end
    echo (printf "\e[1;32m%s\e[m" "\\")
end
string repeat -n (math $x \* 2) "="
echo
echo
echo """
 _   _    _    ____  ______   __
| | | |  / \  |  _ \|  _ \ \ / /
| |_| | / _ \ | |_) | |_) \ V /
|  _  |/ ___ \|  __/|  __/ | |
|_| |_/_/   \_\_|   |_|    |_|

  _   _ _______        __
 | \ | | ____\ \      / /
 |  \| |  _|  \ \ /\ / /
 | |\  | |___  \ V  V /
 |_| \_|_____|  \_/\_/

__   _______    _    ____  _
\ \ / / ____|  / \  |  _ \| |
 \ V /|  _|   / _ \ | |_) | |
  | | | |___ / ___ \|  _ <|_|
  |_| |_____/_/   \_\_| \_(_)

"""
